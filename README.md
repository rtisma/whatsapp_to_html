# WHATSAPP TO HTML Converter Instructions #

**Note1:** These instructions are for Android

**Note2:** SSHDroid only works on WiFi

## A) Convert \*.db.crypt7 to \*.db.crypt (or \*.db) ##
1. Open WhatsApp, go to the main screen, and go click
```
Setting -> Chat Settings -> Backup Conversations
```
2. On Android, download the app [WhatsApp Tri-Crypt](https://play.google.com/store/apps/details?id=com.tricrypt)
3. Run the app (grant root acces). Click the **Decrypt WhatsApp Database** button
4. If succesfully completed, a message should pop up. You should now see  *msgstore.db* (for rooted phones only) or *msgstore.db.crypt* (for unrooted phones)

## B) Start SSHDroid server
1. Download and Install the app [SSHDroid](https://play.google.com/store/apps/details?id=berserker.android.apps.sshdroid)
2. Start SSHDroid.
3. Go to **Options**, and uncheck **Enable root**. Go back to the main screen
4. Start the server if you havent already
5. Note the Address displayed

## C) Generate \*.html file for Linux##
1. Run the following commands:
```
#!bash
git clone http://rtisma@bitbucket.org/rtisma/whatsapp_to_html.git
cd whatsapp_to_html
perl gen_html.pl
```

**Note3:** The script will prompt you for the address from C) followed by the password (which is *admin* by default)

**Note4:** The script copied the WhatsApp/Media folder locally (this can sometimes
be huge) so it may seem like its hanging. Just be patient...


###Links###
http://www.stumbleupon.com/su/4JGvhI

http://forum.xda-developers.com/showthread.php?t=1583021