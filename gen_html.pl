#! /bin/perl

#TODO: Add "required software" sections
# Also, should have unmounting procedure
#
# Required linux packages:
# python
# sshfs
# ssh-client
# fping
# m2crypto
# python-crypto
# git
#
# Required Apps on Android Device:
# WhatsApp Tri-Crypt
# SSHDroid

$script_name = `basename $0`; chomp($script_name);
$script_dir = `dirname $0`;
$script_dir = `readlink -f $script_dir`; chomp($script_dir);
print "script_dir = $script_dir\n";

print "Running script...\n";
print "What is the address from SSHDroid?\n";
my $addr = <STDIN>; chomp ($addr);
$addr =~ s/:.*//;

(my $hostname = $addr) =~ s/.*\@//;
$is_hostname_unreachable = `fping $hostname | grep -c "unreachable" `; chomp ($is_hostname_unreachable);

if ($is_hostname_unreachable)   {
  print ">>ERROR: The hostname \"$hostname\" is not valid. Please use the correct one (ping $hostname)\n";
  die;
}

$dirname = "SSHDroid";
$abs_dirname = "$script_dir/$dirname";

&unmount_remove($abs_dirname);

`mkdir $abs_dirname`;
$cmd = 'sshfs -p 2222 '.$addr.':/ '.$abs_dirname;
print "cmd = $cmd\n";
system($cmd);

$tmp_dir = $script_dir."/generated_html_".$$;
$wa_loc = $abs_dirname.'/storage/emulated/legacy/WhatsApp';
$wa_xtract = "$script_dir/src/whatsapp_xtract.py";


$db_file = $wa_loc.'/Databases/msgstore.db';
if (! -e $db_file)  {
  print ">>ERROR: The DB file \"$db_file\" doesnt exist. Make sure everything is properly mounted and that the Tri-Crypt program was run\n";
  die ;
}
$special_cp = $ENV{HOME}.'/Applications/coreutils-8.21/src/cp -g';
#$cmd = "mkdir $tmp_dir; cd $tmp_dir; ln -s $wa_loc/Media; ln -s $db_file ;  python $wa_xtract msgstore.db "; #This does linking
$cmd = "mkdir $tmp_dir; cd $tmp_dir; cp -r $wa_loc/Media Media ; cp $db_file msgstore.db ;  python $wa_xtract msgstore.db "; 
#$cmd =  "mkdir $tmp_dir; cd $tmp_dir; $special_cp -r $wa_loc/Media Media ; $special_cp $db_file msgstore.db ;  python $wa_xtract msgstore.db "; #This uses "Advanced Copy" utility which shows the copy progression
print "cmd = $cmd\n";
system($cmd);

$got_answer = 0;
while ($got_answer == 0)
{
  print "\n Would you like to unmount SSHDroid directory? (y/n) \n";
  my $answer = <STDIN>; chomp($answer);
  if ($answer == "y" || $answer == "n") {
    $got_answer = 1;
  }
  else  {
    print " The answer \"$answer\" was not correct...try again\n";
  }

}

$umount_cmd = "sudo umount $abs_dirname";
if ($answer == "y") {
  &unmount_remove($abs_dirname);
}
else    {
  print "When ready, run this command manually to unmount:\n$umount_cmd\n\n";
}

print '**** WHATSAPP TO HTML Conversion Complete ****'."\n";

sub unmount_remove 
{
  $abs_dirname = $_[0];
  if (-d $abs_dirname)    {
    $result = `mount -l | grep -c '$abs_dirname' `;
    if ($result >0)   {
      print ">> The directory \"$abs_dirname\" is mounted. Unmounting now...\n";
      print `sudo umount $abs_dirname`;
    }
    print ">> Removing \"$abs_dirname\"...\n";
    print `rm -rf $abs_dirname`;
  }

}



